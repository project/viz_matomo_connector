<?php

namespace Drupal\smart_content_matomo\Plugin\smart_content\Condition\Group;

use Drupal\smart_content\Condition\Group\ConditionGroupBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartConditionGroup(
 *   id = "matomo",
 *   label = @Translation("Matomo"),
 *   weight = 0,
 * )
 */
class Matomo extends ConditionGroupBase {

}