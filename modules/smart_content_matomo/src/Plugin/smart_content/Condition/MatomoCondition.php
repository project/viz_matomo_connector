<?php

namespace  Drupal\smart_content_matomo\Plugin\smart_content\Condition;

use Drupal\smart_content\Condition\ConditionTypeConfigurableBase;

/**
 * Provides a default Smart Condition.
 *
 * @SmartCondition(
 *   id = "matomo",
 *   label = @Translation("Matomo"),
 *   group = "matomo",
 *   weight = 0,
 *   deriver = "Drupal\smart_content_matomo\Plugin\Derivative\MatomoDerivative"
 * )
 */
class MatomoCondition extends ConditionTypeConfigurableBase {

  /**
   * {@inheritdoc}
   */
  public function getLibraries() {
    $libraries = array_unique(array_merge(parent::getLibraries(), ['smart_content_matomo/condition.matomo']));
    return $libraries;
  }

}