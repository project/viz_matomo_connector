<?php

namespace Drupal\smart_content_matomo\Plugin\smart_content\Condition\Type;


use Drupal\Core\Form\FormStateInterface;
use Drupal\smart_content\Annotation\SmartConditionType;
use Drupal\smart_content\Condition\Type\ConditionTypeBase;

/**
 * Provides a 'datetime' ConditionType.
 *
 * @SmartConditionType(
 *  id = "datetime",
 *  label = @Translation("DateTime"),
 * )
 */
class DateTime extends ConditionTypeBase {

  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $condition_definition = $this->conditionInstance->getPluginDefinition();
    $form['op'] = [
      '#type' => 'select',
      '#options' => $this->getOperators(),
      '#default_value' => isset($this->configuration['op']) ? $this->configuration['op'] : $this->defaultFieldConfiguration()['op'],
    ];
    $form['value'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#default_value' => isset($this->configuration['value']) ? $this->configuration['value'] : $this->defaultFieldConfiguration()['value'],
    ];
    if (isset($condition_definition['format_options']['prefix'])) {
      $form['value']['#prefix'] = $condition_definition['format_options']['prefix'];
    }
    if (isset($condition_definition['format_options']['suffix'])) {
      $form['value']['#suffix'] = $condition_definition['format_options']['suffix'];
    }
    return $form;
  }

  public function defaultFieldConfiguration() {
    return [
      'op' => 'equals',
      'value' => '',
    ];
  }

  public function getLibraries() {
    return ['smart_content_matomo/condition_type.datetime'];
  }

  public function getAttachedSettings() {
    return $this->getConfiguration() + $this->defaultFieldConfiguration();
  }

  public function getOperators() {
    return [
      'equals' => '=',
      'gt' => '>',
      'lt' => '<',
      'gte' => '>=',
      'lte' => '<=',
    ];
  }

  public function getHtmlSummary() {
    $config = $this->getConfiguration();
    $operator = $this->getOperators()[$config['op']];

    $output = [
      '#type' => 'markup',
      'op' => [
        '#markup' => "{$operator}",
        '#prefix' => '<span class="condition-type-op">',
        '#suffix' => '</span> ',
      ],
      'value' => [
        '#markup' => $config['value'],
        '#prefix' => '<span class="condition-type-value">"',
        '#suffix' => '"</span>',
      ],
    ];
    return $output;
  }


}