<?php

namespace Drupal\smart_content_matomo\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

class MatomoDerivative extends DeriverBase {

  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [
      'first_visit' => [
        'label' => 'First Visit',
        'type' => 'boolean',
      ] + $base_plugin_definition,
      'visit_count' => [
        'label' => 'Visit Count',
        'type' => 'number',
      ] + $base_plugin_definition,
      'total_page_views' => [
        'label' => 'Total Page Views',
        'type' => 'number',
      ] + $base_plugin_definition,
      'unique_page_views' => [
        'label' => 'Unique Page Views',
        'type' => 'number',
      ] + $base_plugin_definition,
      'time_on_site' => [
        'label' => 'Time on Site',
        'type' => 'number',
      ] + $base_plugin_definition,
      'first_visit_time' => [
        'label' => 'First Visit Time',
        'type' => 'datetime',
      ] + $base_plugin_definition,
      'last_visit_time' => [
        'label' => 'Last Visit Time',
        'type' => 'datetime'
      ] + $base_plugin_definition,
      'last_visit_location' => [
        'label' => 'Last Visit Location',
        'type' => 'textfield'
      ]

    ];

    return $this->derivatives;
  }

}