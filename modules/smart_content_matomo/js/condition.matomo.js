/**
 * @file
 * Provides condition values for all browser conditions.
 */

(function (Drupal, drupalSettings) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.Field = Drupal.smartContent.plugin.Field || {};
  let visitor_info = drupalSettings.visitor_info;

  Drupal.smartContent.plugin.Field['analysis:first_visit'] = function (condition) {
    if (visitor_info === false) {
      return true;
    }
    let totalVisits = visitor_info.totalVisits;
    if (totalVisits > 1) {
      return false;
    } else {
      return true;
    }
  };

  Drupal.smartContent.plugin.Field['analysis:visit_count'] = function (condition) {
    if (visitor_info === false) {
      return 0;
    }
    return visitor_info.totalVisits;
  };

  Drupal.smartContent.plugin.Field['analysis:total_page_views'] = function (condition) {
    if (visitor_info === false) {
      return 0;
    }
    return visitor_info.totalPageViews;
  };

  Drupal.smartContent.plugin.Field['analysis:unique_page_views'] = function (condition) {
    if (visitor_info === false) {
      return 0;
    }
    return visitor_info.totalUniquePageViews;
  };

  Drupal.smartContent.plugin.Field['analysis:time_on_site'] = function (condition) {
    if (visitor_info === false) {
      return 0;
    }
    return visitor_info.totalVisitDurationPretty;
  };

  Drupal.smartContent.plugin.Field['analysis:first_visit_time'] = function (condition) {
    if (visitor_info === false) {
      return false;
    }
    return visitor_info.firstVisit.date;
  };

  Drupal.smartContent.plugin.Field['analysis:last_visit_time'] = function (condition) {
    if (visitor_info === false) {
      return false;
    }
    return visitor_info.lastVisit.date;
  };

  Drupal.smartContent.plugin.Field['analysis:last_visit_location'] = function (condition) {
    if (visitor_info === false) {
      return '';
    }
    return visitor_info.countries[0].prettyName;
  };

})(Drupal, drupalSettings);
