/**
 * @file
 * Client-side evaluation for all condition types provided by Smart Content.
 */

(function (Drupal) {

  Drupal.smartContent = Drupal.smartContent || {};
  Drupal.smartContent.plugin = Drupal.smartContent.plugin || {};
  Drupal.smartContent.plugin.ConditionType = Drupal.smartContent.plugin.ConditionType || {};

  Drupal.smartContent.plugin.ConditionType['type:datetime'] = function (condition, value) {
    if (value == null) {
      return false;
    }
    let visit_time = new Date(parseInt(value) * 1000);
    let Year = visit_time.getFullYear();
    let month = (parseInt(visit_time.getMonth()) + 1) > 9 ? visit_time.getMonth() + '' : '0' + (visit_time.getMonth() + 1);
    let day  = (parseInt(visit_time.getDate()) + 1) > 9 ? visit_time.getDate() + '' : '0' + (visit_time.getDate() + 1);
    let time = parseInt(Year + month + day);
    switch (condition.settings['op']) {
      case 'equals':
        return (Number(time) === Number(condition.settings['value']));

      case 'gt':
        return (Number(time) > Number(condition.settings['value']));

      case 'lt':
        return (Number(time) < Number(condition.settings['value']));

      case 'gte':
        return (Number(time) >= Number(condition.settings['value']));

      case 'lte':
        return (Number(time) <= Number(condition.settings['value']));
    }
    return false;
  };

})(Drupal);
