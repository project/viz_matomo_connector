<?php

namespace Drupal\viz_matomo_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class AnalysisSettingsForm extends ConfigFormBase {

  public function getFormId() {
    return 'analysis_config_form';
  }

  public function getEditableConfigNames() {
    return ['viz_matomo_connector.settings'];
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('viz_matomo_connector.settings');

    $form['#title'] = 'Analysis Settings';

    $form['site_id'] = [
      '#type' => 'textfield',
      '#title' => 'Site ID',
      '#default_value' => $config->get('site_id'),
      '#description' => 'Site ID, used to distinguish different sites',
    ];
    $form['token'] = [
      '#type' => 'textfield',
      '#title' => 'Token',
      '#default_value' => $config->get('token'),
      '#description' => 'Token, used for verification',
    ];
    $form['url'] = [
      '#type' => 'textfield',
      '#title' => 'URL',
      '#default_value' => $config->get('url'),
      '#description' => 'URL',
    ];

    return parent::buildForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $site_id = $form_state->getValue('site_id');
    $token = $form_state->getValue('token');
    $url = $form_state->getValue('url');
    $this->config('viz_matomo_connector.settings')
      ->set('site_id', $site_id)
      ->set('token', $token)
      ->set('url', $url)
      ->save();
    parent::submitForm($form, $form_state);
  }

}