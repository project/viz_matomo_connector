<?php

namespace Drupal\viz_matomo_connector\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block (
 *   id = "visitor_info_block",
 *   admin_label = @Translation("Visitor Info Block"),
 *   category = "QY"
 * )
 */
class VisitorInfoBlock extends BlockBase {

  public function build() {
    $connector = \Drupal::service('viz_matomo_connector');
    $visitor_info = $connector->getVisitorInfo();
    if ($visitor_info == False) {
      $first = 1;
    } else {
      $first = 0;
    }

    return [
      '#theme' => 'visitor_info',
      '#data' => [
        'first' => $first,
        'info' => $visitor_info
      ],
      '#attached' => [
        'library' => 'viz_matomo_connector/visitor_info'
      ]
    ];
  }

}