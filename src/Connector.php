<?php

namespace Drupal\viz_matomo_connector;

class Connector {

  protected $config;

  protected $method;

  protected $params;

  protected $default_params;

  protected $client;

  protected $response;
  protected $action;

  public function __construct () {
    $this->getConfig();
    $this->setDefaultParams();
    $this->getClient();
  }

  protected function getConfig () {
    $config = \Drupal::config('viz_matomo_connector.settings');
    $this->config = $config;
  }

  public function setParam ($params) {
    $this->params = $params;
  }

  public function setMethod ($method) {
    $this->method = $method;
  }

  public function getVisitorInfo ($visitor_id = null) {
    if (isset($this->response) && $this->action === 'getVisitorInfo') {
      return $this->response;
    }
    if (!$visitor_id) {
      $cookie = $this->getCookie();
      $cookie_name = '_pk_id.' . $this->config->get('site_id');
      foreach ($cookie as $key => $value) {
        if (strpos($key, $cookie_name) !== False) {
          $visitor_id = explode('.', $value)[0];
        }
      }
      if (!$visitor_id) return False;
    }
    $this->setMethod('Live.getVisitorProfile');
    $params = [
      'visitorId' => $visitor_id
    ];
    $this->setParam($params);
    try {
      $response = $this->action();
      $this->response = $response;
      $this->action = 'getVisitorInfo';
    } catch (\Exception $e) {
      return False;
    }

    return $response;
  }

  public function action () {
    $response = $this->getResponse();
    return $response;
  }

  protected function setDefaultParams () {
    $params = [
      'idSite' => $this->config->get('site_id'),
      'token_auth' => $this->config->get('token'),
      'format' => 'JSON',
      'module' => 'API',
    ];
    $this->default_params = $params;
  }

  protected function getResponse () {
    $url = $this->getUrl();

    $response = $this->client->request('GET', $url, $this->options);
    $content = $response->getBody()->getContents();
    $json = \GuzzleHttp\json_decode($content, True);
    return $json;
  }

  protected function getUrl () {
    $base_url = $this->config->get('url');
    $params = array_merge_recursive($this->params, $this->default_params);
    $params['method'] = $this->method;
    $params_string = \GuzzleHttp\http_build_query($params);
    $url = $base_url . '?' . $params_string;
    return $url;
  }

  protected function getClient () {
    $client = \Drupal::httpClient();
    $default_options = [
      'headers' => [
        'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Language' => 'zh-CN,zh;q=0.9,zh-Hans;q=0.8',
      ],
      'timeout'  => 20,
    ];
    $this->client = $client;
    $this->options = $default_options;
  }

  protected function getCookie () {
    $request = \Drupal::request();
    $header = $request->headers;
    $cookie = $header->get('cookie');
    if (!$cookie) {
      return False;
    }
    $parts = explode(';', $cookie);
    $cookie_list = [];
    foreach ($parts as $part) {
      if (!strpos($part, '=')) {
        continue;
      }
      list($key, $value) = explode('=', trim($part));
      $cookie_list[$key] = $value;
    }
    return $cookie_list;
  }

}