<?php

namespace Drupal\viz_matomo_connector\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\viz_matomo_connector\AnalysisHandler;
use Drupal\viz_matomo_connector\Connector;

class ApiController extends ControllerBase {

  /**
   * @var \Drupal\viz_matomo_connector\Connector
   */
  protected $connector;

  public function __construct(Connector $connector) {
    $this->connector = $connector;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('viz_matamo_connector'),
    );
  }

  public function index() {}

  public function getCookie () {
    $request = \Drupal::request();
    $header = $request->headers;
    $cookie = $header->get('cookie');
    if (!$cookie) {
      return False;
    }
    $parts = explode(';', $cookie);
    $cookie_list = [];
    foreach ($parts as $part) {
      if (!strpos($part, '=')) {
        continue;
      }
      list($key, $value) = explode('=', trim($part));
      $cookie_list[$key] = $value;
    }
    return $cookie_list;
  }

}